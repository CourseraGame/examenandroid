package pearcompany.androidexam.Utils;

/**
 * Created by pascpaul on 2/2/17.
 */

public interface ConnectivityListener {
    enum CONNECTIVITY {
        CONNECTED,
        DISCONNECTED
    }

    void onConnectivityChanged(CONNECTIVITY status);
}


