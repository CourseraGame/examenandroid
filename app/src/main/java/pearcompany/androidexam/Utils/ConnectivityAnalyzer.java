package pearcompany.androidexam.Utils;

/**
 * Created by pascpaul on 2/2/17.
 */

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.TelephonyManager;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Calin Martinconi on 10/12/15.
 */
@Singleton
public class ConnectivityAnalyzer extends BroadcastReceiver {

    public static final int WIFI_SIGNAL_STRENGTH_MAX = 100;
    private static final int WIFI_SIGNAL_STRENGTH_MIN = 35;

    private ConnectivityListener currentListener;

    private Application application;

    private boolean registered;

    @Inject
    public ConnectivityAnalyzer(Application application) {
        this.application = application;
    }

    private int getWifiStrength(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        int linkSpeed = wifiManager.getConnectionInfo().getRssi();

        return WifiManager.calculateSignalLevel(linkSpeed, WIFI_SIGNAL_STRENGTH_MAX);
    }

    private boolean weakWifiStrength(Context context) {
        if (isWifiConnected(context)) {
            int wifiSignalStrength = getWifiStrength(context);

            return wifiSignalStrength < WIFI_SIGNAL_STRENGTH_MIN;
        }

        return false;
    }

    private boolean weakNetworkSignal(Context context) {
        return weakNetworkSignal(context, 3, 3, 3, 3);
    }

    private boolean weakNetworkSignal(Context context, int gsmLevel, int cdmaLevel, int lteLevel, int wcmaLevel) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            List<CellInfo> cellInfoList = telephonyManager.getAllCellInfo();

            if ((cellInfoList != null) && (cellInfoList.size() > 0)) {
                CellInfo info = cellInfoList.get(0);

                if (info instanceof CellInfoGsm) {
                    return ((CellInfoGsm) info).getCellSignalStrength().getLevel() < gsmLevel;
                } else if (info instanceof CellInfoCdma) {
                    return ((CellInfoCdma) info).getCellSignalStrength().getLevel() < cdmaLevel;
                } else if (info instanceof CellInfoLte) {
                    return ((CellInfoLte) info).getCellSignalStrength().getLevel() < lteLevel;
                } else if (info instanceof CellInfoWcdma) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        return ((CellInfoWcdma) info).getCellSignalStrength().getLevel() < wcmaLevel;
                    }
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    private boolean isWeakWifiConnected(Context context) {
        return (isWifiConnected(context) && weakWifiStrength(context));
    }

    private boolean isWeakNetworkConnected(Context context) {
        return (isNetworkConnected(context) && weakNetworkSignal(context));
    }

    public boolean isWifiConnected(Context context) {
        NetworkInfo info = getActiveNetworkInfo(context);
        return ((info != null) && info.isConnected() && (info.getType() == ConnectivityManager.TYPE_WIFI));
    }

    public boolean isNetworkConnected(Context context) {
        NetworkInfo info = getActiveNetworkInfo(context);
        return ((info != null) && info.isConnected() && (info.getType() == ConnectivityManager.TYPE_MOBILE));
    }

    public boolean isConnected() {
        NetworkInfo info = getActiveNetworkInfo(application);
        return ((info != null) && info.isConnected());
    }

    public boolean isWeakConnection(Context context) {
        return (isWeakWifiConnected(context) || isWeakNetworkConnected(context));
    }

    private NetworkInfo getActiveNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public void setListener(ConnectivityListener listener) {
        if (listener != null) {
            this.currentListener = listener;
            registerReceiver();
        }
    }

    public void removeCurrentListener() {
        unregisterReceiver();
        this.currentListener = null;
    }

    public void removeListener(ConnectivityListener listener) {
        if (listener == currentListener) {
            removeCurrentListener();
        }
    }

    private void registerReceiver() {
        if (!registered) {

            application.registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            registered = true;
        }
    }

    private void unregisterReceiver() {
        if (registered) {

            application.unregisterReceiver(this);
            registered = false;
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (currentListener != null) {
            currentListener.onConnectivityChanged(getConnectivityStatus());
        }
    }

    private ConnectivityListener.CONNECTIVITY getConnectivityStatus() {
        if (isConnected()) {
            return ConnectivityListener.CONNECTIVITY.CONNECTED;
        } else {
            return ConnectivityListener.CONNECTIVITY.DISCONNECTED;
        }
    }

    public boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

}

