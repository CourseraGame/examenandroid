package pearcompany.androidexam.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import pearcompany.androidexam.R;
import pearcompany.androidexam.model.Good;
import pearcompany.androidexam.model.Note;
import pearcompany.androidexam.model.ResponseNote;

/**
 * Created by pascpaul on 2/2/17.
 */

public class MyListAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<Good> goods;

    public MyListAdapter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return goods.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.cell_layout, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.textId);
        textView.setText(goods.get(position).getName());

        return convertView;
    }

    public void setGoods(List<Good> goods) {
        this.goods = goods;
    }
}
