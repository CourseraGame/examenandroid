package pearcompany.androidexam.services;

import com.squareup.okhttp.ResponseBody;

import java.util.List;

import pearcompany.androidexam.model.Good;
import pearcompany.androidexam.model.GoodModel;
import pearcompany.androidexam.model.ResponseNote;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by pascpaul on 2/2/17.
 */

public interface ApiService {

    //@GET("/note")
    //Observable<ResponseNote> getNotes(@Header("If-Modified-Since") String lastModified, @Query("page") int page);

    @DELETE("/note/{id}")
    Observable<ResponseBody> deleteNote(@Path("id") int id);

    @GET("/note")
    Observable<ResponseNote> getNotes(@Query("page") int page);


    @GET("/Good")
    Observable<List<Good>> getGoods(@Query("lastUpdated") int lastUpdated);

    @GET("/Good")
    Observable<List<Good>> getGoods();

    @POST("/Good/buy")
    Observable<Good> buyGood(@Body GoodModel good);

    @POST("/Good/sell")
    Observable<Good> sellGood(@Body GoodModel good);



}
