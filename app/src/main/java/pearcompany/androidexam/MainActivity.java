package pearcompany.androidexam;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pearcompany.androidexam.Utils.ConnectivityAnalyzer;
import pearcompany.androidexam.Utils.ConnectivityListener;
import pearcompany.androidexam.adapters.MyListAdapter;
import pearcompany.androidexam.manager.TransactionsManager;
import pearcompany.androidexam.model.Good;
import pearcompany.androidexam.model.Note;
import pearcompany.androidexam.presenter.NotesPresenter;
import pearcompany.androidexam.services.ApiService;
import pearcompany.androidexam.views.NotesView;
import pearcompany.androidexam.manager.CacheManager;
import pearcompany.androidexam.model.ResponseNote;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_main)
public class MainActivity extends RoboActivity implements NotesView, ConnectivityListener {

    @Inject
    private NotesPresenter notesPresenter;

    @Inject
    private TransactionsManager transactionsManager;

    @Inject
    private ConnectivityAnalyzer connectivityAnalyzer;

    @InjectView(R.id.listView)
    private ListView listView;

    @InjectView(R.id.sellId)
    private Button sellButton;

    @InjectView(R.id.transactionsTextView)
    private TextView transactionTextView;

    private MyListAdapter adapter;

    private List<Good> goods;

    private int lastUpdated = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectivityAnalyzer.setListener(this);

        goods = new ArrayList<>();

        notesPresenter.attachView(this);
        adapter = new MyListAdapter(this);
        adapter.setGoods(new ArrayList<>());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            String good = "";
            Gson gson = new Gson();
            good = gson.toJson(goods.get(position));

            Intent detailsActivity = new Intent(this, DetailActivity.class);
            detailsActivity.putExtra("good", good);
            detailsActivity.putExtra("sell", "false");
            startActivity(detailsActivity);


        });

        sellButton.setOnClickListener(v -> {
            Intent detailsActivity = new Intent(this, DetailActivity.class);
            detailsActivity.putExtra("good", "");
            detailsActivity.putExtra("sell", "true");
            startActivity(detailsActivity);
        });

        notesPresenter.getGoods(lastUpdated);

    }

    @Override
    protected void onResume() {

        transactionTextView.setText(transactionsManager.getTransaction());

        super.onResume();
    }

    @Override
    public void onDestroy(){
        connectivityAnalyzer.removeCurrentListener();
    }

    @Override
    public void onLoadPageOneError(String message) {

    }

    @Override
    public void setGoods(List<Good> goods) {
        this.goods = goods;
        adapter.setGoods(goods);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onConnectivityChanged(CONNECTIVITY status) {
        if(status == CONNECTIVITY.CONNECTED){

        } else {

        }
    }


}
