package pearcompany.androidexam.manager;

import java.util.List;

import javax.inject.Inject;

import pearcompany.androidexam.model.Good;
import pearcompany.androidexam.model.ResponseNote;
import pearcompany.androidexam.services.ApiService;
import rx.Observable;

/**
 * Created by pascpaul on 2/3/17.
 */

public class GoodsManager {

    private ApiService apiService;
    private CacheManager cacheManager;

    @Inject
    public GoodsManager(CacheManager cacheManager, ApiService apiService){
        this.apiService = apiService;
        this.cacheManager = cacheManager;
    }

    public Observable<List<Good>> getAllGoods(int value){
        return apiService.getGoods(500);
    }

}

/*
 private void setLastTravelPreferencesResult(List<TravelPreferencesResult> travelPreferencesResult) {
        cacheManager.putObject(getTravelPreferencesCacheKey(), travelPreferencesResult);
    }

    public List<TravelPreferencesResult> getLastTravelpreferencesResult() {
        return cacheManager.getListEvenExpired(getTravelPreferencesCacheKey(), getListType());
    }

    private Type getListType() {
        return (new TypeToken<ArrayList<TravelPreferencesResult>>() {
        }.getType());
    }
 */
