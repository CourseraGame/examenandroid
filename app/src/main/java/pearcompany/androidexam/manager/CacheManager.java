package pearcompany.androidexam.manager;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import st.lowlevel.storo.Storo;

@Singleton
public class CacheManager {

    public static final String EXPIRATION_KEY = "_EXPIRATION";

    private static final long NO_EXPIRATION = 0;

    private String userId;

    @Inject
    public CacheManager() {
    }

    public void setCurrentUser(String userId) {
        this.userId = userId;
    }

    public <T> List<T> getListEvenExpired(String key, Type type) {
        return (List<T>) Storo.get(key, type).execute();
    }

    public <T> List<T> getListUnexpired(String key, Type type) {
        List<T> list = (ArrayList<T>) getListEvenExpired(key, type);

        if (hadExpired(key)) {
            return null;
        } else {
            return list;
        }
    }

    public <T> Observable<T> getObjectObservableUnexpired(String key, Class<T> classOfT) {
        return getObjectObservableEvenExpired(key, classOfT).filter(object -> !hadExpired(key));
    }

    public <T> Observable<T> getObjectObservableEvenExpired(String key, Class<T> classOfT) {
        return Observable.fromCallable(() -> getObjectEvenExpired(key, classOfT));
    }

    public <T> T getObjectUnexpired(String key, Class<T> classOfT) {
        if (!hadExpired(key)) {
            return getObjectEvenExpired(key, classOfT);
        } else {
            return null;
        }
    }

    public <T> T getObjectEvenExpired(String key, Class<T> classOfT) {
        return Storo.get(key, classOfT).execute();
    }

    public <T> void putObject(String key, T object) {
        putObject(key, object, NO_EXPIRATION);
    }

    public <T> void putObject(String key, T object, long expirationMillis) {
        boolean result = Storo.put(key, object).execute();
        if (!result) {
        }

        if (expirationMillis != NO_EXPIRATION) {
            result = Storo.put(getExpirationKey(key), getExpirationTime(expirationMillis)).execute();
            if (!result) {
            }
        }
    }

    private long getExpirationTime(long expirationMillis) {
        return (expirationMillis == NO_EXPIRATION ? expirationMillis
                : System.currentTimeMillis() + expirationMillis);
    }

    private <T> boolean hadExpired(String key) {
        Long expiration = Storo.get(getExpirationKey(key), Long.class).execute();

        return ((expiration != null) && (expiration < System.currentTimeMillis()));
    }

    private String getExpirationKey(String key) {
        return (key + EXPIRATION_KEY);
    }

    public void deleteObject(String key) {
        Storo.delete(key);
    }

    public void clearCache() {
        Storo.clear();
    }
}
