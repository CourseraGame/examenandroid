package pearcompany.androidexam.manager;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pearcompany.androidexam.model.Good;

/**
 * Created by pascpaul on 2/3/17.
 */

public class TransactionsManager {

    private CacheManager cacheManager;
    private SharedPreferences sharedPreferences;
    private static final String TRANSACTION = "TRANSACTIONS_KEY";//key pt cumparari

    @Inject
    public TransactionsManager(CacheManager cacheManager, SharedPreferences sharedPreferences){
        this.cacheManager = cacheManager;
        this.sharedPreferences = sharedPreferences;
    }

    /*
    public void setSavedDate() {
        sharedPreferences.edit().putString(CURRENT_DATE, getDate()).apply();
    }

    public String getSavedDate() {
        return sharedPreferences.getString(CURRENT_DATE, UNSET);
    }
     */

    public void saveNewTransaction(Good good, String type){
        String end = "";
        String t = getTransaction();
        end += t;
        end += '\n';
        end += good.toString();
        end += type;
        sharedPreferences.edit().putString(TRANSACTION, end).apply();

    }

    public String getTransaction(){
        return sharedPreferences.getString(TRANSACTION, "");
    }


    public List<Good> getTransactions(){
        if(cacheManager.getListEvenExpired(TRANSACTION, getListType()) == null){
            return new ArrayList<>();
        }
        return cacheManager.getListEvenExpired(TRANSACTION, getListType());
    }

    public void addTransaction(Good good){
        List<Good> transactions = getTransactions();
        if(transactions == null){
            transactions = new ArrayList<>();
        }
        transactions.add(good);
        cacheManager.putObject(TRANSACTION, transactions);

        Log.d("testare", transactions.size() + "");
    }

    private Type getListType() {
        return (new TypeToken<ArrayList<Good>>() {
        }.getType());
    }
}
