package pearcompany.androidexam.manager;

import javax.inject.Inject;
import javax.inject.Singleton;

import pearcompany.androidexam.model.ResponseNote;
import pearcompany.androidexam.services.ApiService;
import rx.Observable;

/**
 * Created by pascpaul on 2/2/17.
 */

@Singleton
public class NoteManager {

    private ApiService apiService;
    private CacheManager cacheManager;
    private static final String NOTES_KEY = "NOTES";

    @Inject
    public NoteManager(CacheManager cacheManager, ApiService apiService){
        this.apiService = apiService;
        this.cacheManager = cacheManager;
    }

    public Observable<ResponseNote> getNotes(int page){
        return apiService.getNotes(page);//.doOnNext(this::saveNotesToCache);
    }

    private void saveNotesToCache(ResponseNote notes){
        if(notes == null){
            cacheManager.putObject(NOTES_KEY, notes);
        } else {
            ResponseNote r = getNotesFromCache();
            notes.append(r.getNotes());
            cacheManager.putObject(NOTES_KEY, notes);
        }

    }

    private ResponseNote getNotesFromCache(){
        return cacheManager.getObjectEvenExpired(NOTES_KEY, ResponseNote.class);
    }





}
