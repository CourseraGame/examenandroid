package pearcompany.androidexam.model;

import java.io.Serializable;

/**
 * Created by ioaneusebiu on 2/3/17.
 */

public class Good implements Serializable {

    private int id;
    private String name;
    private int quantity;
    private int price;
    private long updated;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public long getUpdated() {
        return updated;
    }

    public void setUpdated(int updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return id + "," + name + "," + quantity + "," + price;
    }
}
