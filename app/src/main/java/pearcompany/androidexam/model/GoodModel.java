package pearcompany.androidexam.model;

import java.io.Serializable;

/**
 * Created by pascpaul on 2/3/17.
 */

public class GoodModel implements Serializable {

    private String name;
    private Integer quantity;
    private Integer price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
