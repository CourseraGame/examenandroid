package pearcompany.androidexam.model;

import java.util.List;

/**
 * Created by pascpaul on 2/2/17.
 */

public class ResponseNote {
    private int page;
    private List<Note> notes;
    private boolean more;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public boolean isMore() {
        return more;
    }

    public void setMore(boolean more) {
        this.more = more;
    }

    public void append(List<Note> n){
        notes.addAll(n);
    }
}
