package pearcompany.androidexam.views;

import java.util.List;

import pearcompany.androidexam.model.Good;
import pearcompany.androidexam.model.ResponseNote;

/**
 * Created by pascpaul on 2/2/17.
 */

public interface NotesView {

    void onLoadPageOneError(String message);

    void setGoods(List<Good> goods);
}
