package pearcompany.androidexam.common;

import com.google.inject.AbstractModule;

import pearcompany.androidexam.services.ApiService;
import pearcompany.androidexam.services.ApiServiceProvider;
import roboguice.inject.SharedPreferencesName;

public class Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(ApiService.class).toProvider(ApiServiceProvider.class);
        bindConstant().annotatedWith(SharedPreferencesName.class).to("preferences");

        requestStaticInjection(Module.class);
    }
}
