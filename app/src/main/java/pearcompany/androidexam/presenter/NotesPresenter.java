package pearcompany.androidexam.presenter;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import pearcompany.androidexam.manager.GoodsManager;
import pearcompany.androidexam.model.Good;
import pearcompany.androidexam.views.NotesView;
import pearcompany.androidexam.manager.NoteManager;
import pearcompany.androidexam.model.ResponseNote;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by pascpaul on 2/2/17.
 */

public class NotesPresenter {

    private GoodsManager goodsManager;
    private NotesView view;

    @Inject
    public NotesPresenter(GoodsManager goodsManager){
        this.goodsManager = goodsManager;
    }

    public void attachView(NotesView view){
        this.view = view;
    }

    public void getGoods(int value){
        goodsManager.getAllGoods(value)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<Good>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        view.onLoadPageOneError(e.getMessage());

                        Log.d("salutare", e.getMessage());
                    }

                    @Override
                    public void onNext(List<Good> goods) {
                        view.setGoods(goods);
                        Log.d("salutare", goods.size() +"");

                    }
                });
    }

}
