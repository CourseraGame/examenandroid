package pearcompany.androidexam;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.okhttp.ResponseBody;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pearcompany.androidexam.manager.CacheManager;
import pearcompany.androidexam.manager.TransactionsManager;
import pearcompany.androidexam.model.Good;
import pearcompany.androidexam.model.GoodModel;
import pearcompany.androidexam.services.ApiService;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DetailActivity extends RoboActivity {

    @Inject
    private TransactionsManager transactionsManager;

    @InjectView(R.id.goalText)
    private TextView goalText;

    @InjectView(R.id.text1)
    private EditText name;

    @InjectView(R.id.text2)
    private EditText quantity;

    @InjectView(R.id.text3)
    private EditText price;

    @InjectView(R.id.sendButton)
    private Button sendButton;

    @Inject
    private ApiService apiService;

    private String sellString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Gson gson = new Gson();
        Intent intent = getIntent();
        String good = intent.getStringExtra("good");
        sellString = intent.getStringExtra("sell");

        if(sellString.equals("true")){

        } else {
            Good goodObj = new Good();
            goodObj = gson.fromJson(good, Good.class);

            Log.d("super",goodObj.getName());

            goalText.setText(good);
        }

        sendButton.setOnClickListener(v -> {
            String type = name.getText().toString();
            Integer quantity = Integer.parseInt(this.quantity.getText().toString());
            Integer price = Integer.parseInt(this.price.getText().toString());

            GoodModel goodModel = new GoodModel();
            goodModel.setName(type);
            goodModel.setQuantity(quantity);
            goodModel.setPrice(price);

            if(sellString.equals("false")){

                apiService.buyGood(goodModel)
                        .doOnNext(t -> saveTransaction(t, "buy"))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .unsubscribeOn(Schedulers.io())
                        .subscribe(new Subscriber<Good>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(Good good) {
                                Log.d("abecedar", good.getName());
                            }
                        });
            } else {
                    apiService.sellGood(goodModel)
                            .doOnNext(t -> saveTransaction(t, "sell"))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .unsubscribeOn(Schedulers.io())
                            .subscribe(new Subscriber<Good>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onNext(Good good) {
                                    Log.d("salutareeee", good.toString());
                                }
                            });

            }

        });


    }

    private void saveTransaction(Good good, String type) {
        //transactionsManager.addTransaction(good);
        transactionsManager.saveNewTransaction(good, type);
    }
}
