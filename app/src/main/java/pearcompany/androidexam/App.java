package pearcompany.androidexam;
import android.app.Application;
import android.content.Context;

import roboguice.RoboGuice;
import st.lowlevel.storo.StoroBuilder;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initializeApplication();
    }

    public void initializeApplication() {
        storoActivation(); //database
        dependencyInjectionActivation(); //roboguice
    }

    private void dependencyInjectionActivation() {
        RoboGuice.setUseAnnotationDatabases(false);
        RoboGuice.injectMembers(this, this);
    }

    private void storoActivation() {
        StoroBuilder.configure(10 * 1024 * 1024)  // maximum size to allocate in bytes
                .setDefaultCacheDirectory(this)
                .initialize();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

}
